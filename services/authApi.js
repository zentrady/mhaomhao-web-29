import HttpRequest from './http_request'

export default class AuthAPI extends HttpRequest {
  postLogin (body) {
    this.removeHeader()
    return this.create('/auction/api-token-auth/', body)
  }
  postRegister (body) {
    this.removeHeader()
    return this.create('/account/register/', body)
  }
  getMe (auth) {
    this.setHeaderAuth(auth)
    return this.fetch('/account/me/')
  }
  editMe (body, auth) {
    this.setHeaderAuth(auth)
    return this.updateMultipart('/account/me/', body)
  }
  updateAddress (body, auth) {
    this.setHeaderAuth(auth)
    return this.update('/account/address/', body)
  }
}
