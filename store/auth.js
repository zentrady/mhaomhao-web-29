import { make } from 'vuex-pathify'
import { AuthService } from '../services'

const Cookie = process.client ? require('js-cookie') : undefined

export const state = () => ({
  user: null,
  token: null
})

export const actions = {
  async login ({ commit }, body) {
    try {
      const auth = await AuthService.postLogin(body)
      commit('SET_TOKEN', auth.token)
      Cookie.set('auth', auth.token)
    } catch (error) {
      return Promise.reject(error)
    }
  },
  async register ({ commit }, body) {
    try {
      await AuthService.postRegister(body)
    } catch (error) {
      return Promise.reject(error)
    }
  },
  async fetchMe ({ state: { token }, commit }) {
    try {
      const result = await AuthService.getMe(token)
      commit('SET_USER', result)
    } catch (error) {
      return Promise.reject(error)
    }
  },
  async editMe ({ state: { token } }, body) {
    try {
      await AuthService.editMe(body, token)
    } catch (error) {
      return Promise.reject(error)
    }
  },
  async updateAddress ({ state: { token } }, body) {
    try {
      await AuthService.updateAddress(body, token)
    } catch (error) {
      return Promise.reject(error)
    }
  }
}

export const mutations = make.mutations(state)
